Rails.application.routes.draw do
  get 'home_nha', to: 'home#index', as: 'nhanguyen'
  root 'welcome#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
